# coding: utf-8
# mapper.rb


#小文字化及び記号除外関数
def tr_word(x)
	x.chomp!
	x.downcase!
	x.gsub!(/[?,!,#]/,"")
	x
end

#stop-word除外関数

stop_hash = Hash.new{|h, k| h[k] = 0 }

	File.foreach("stop-word.list") do |stop|
		stop.chomp!
		stop_hash[stop] = 1
	end


STDIN.each do |line|
	line.split.each do |word|
		word = tr_word(word)
		if word == ""
			next
		end
		
		if stop_hash[word] != 1
			puts "#{word}\t1"
		end		
	end
end
























