# reducer.rb
data = Hash.new {|h, k| h[k] = 0 }

STDIN.each do |line|
  key, value = line.split(/\t/)
  data[key] += value.to_i
end

data.each do |k, v|
  puts "#{k}\t#{v}"
end