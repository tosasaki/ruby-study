# coding: utf-8

# Studentクラスを作る
class Student
	def initialize(name,age)
		@name = name
		@age  = age
	end
	
	def to_s
		"名前：#{@name}  " + "年齢：#{@age}"
	end
end


shin    = Student.new("久保秋　真",45)
hiroshi = Student.new("久保秋　博",41)

puts shin.to_s
puts hiroshi.to_s