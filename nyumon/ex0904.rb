# coding: utf-8

#Studentクラスを作成する
class Student

	#初期化メソッド
	def initialize(name,age)
		@name = name
		@age  = age
	end

	#ゲッター、セッター定義
	attr_accessor :name, :age
end

#インスタンス作成
tomo = Student.new("佐々木智弘" ,27)
mari = Student.new("原田真里"   ,26)

#ゲッター確認
puts "名前：#{tomo.name}　、年齢：#{tomo.age}"
puts "名前：#{mari.name}　、年齢：#{mari.age}"


#セッター確認
tomo.name = "智弘"
tomo.age  = 20

mari.name = "真里真里"
mari.age  = 16

puts "名前：#{tomo.name}　、年齢：#{tomo.age}"
puts "名前：#{mari.name}　、年齢：#{mari.age}"
