# coding: utf-8

#Studentクラスを作成する
class Student

	#初期化メソッド
	def initialize(name,age)
		@name = name
		@age  = age
	end
	
	#ゲッター(name)メソッド
	def name()
		@name
	end
	
	#ゲッター(age)メソッド
	def age()
		@age
	end
	
	#to_sメソッド
	def to_s()
		"#{@name} , #{@age}"
	end
	
end

#インスタンス作成

tomo = Student.new("佐々木智弘" ,27)
mari = Student.new("原田真里"   ,26)


puts "名前：#{mari.name}　、年齢：#{mari.age}"
