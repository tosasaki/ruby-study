# coding: utf-8
# 表示したい蔵書データを作成する。
require 'date'

publish_date  = Date.new(2005,1,25)
eng_mon_name_long  = Date::MONTHNAMES[publish_date.mon]
eng_mon_name_short = Date::ABBR_MONTHNAMES[publish_date.mon]

puts "出版年：" + publish_date.year.to_s  + "年"
puts "出版月：" + eng_mon_name_long
puts "出版月：" + eng_mon_name_short
