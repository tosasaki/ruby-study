# coding: utf-8
# 表示したい蔵書データを作成する。

title      = "実践アジャイル ソフトウェア開発法とプロジェクト管理"
author     = "山田　正樹"
yomi       = "ヤマダ　マサキ"
publisher  = "ソフトリサーチセンター"

puts "書籍名  ：" + title
puts "著者名  ：" + author
puts "ヨミガナ：" + yomi
puts "出版社  ：" + publisher