# coding: utf-8

#Studentクラスを作成する
class Student

	#初期化メソッド
	def initialize(name,age)
		@name = name
		@age  = age
	end
	
	#ゲッター(name)メソッド
	def name()
		@name
	end
	
	#ゲッター(age)メソッド
	def age()
		@age
	end

	#セッター(name)メソッド
	def name=(i)
		@name = i
	end

	#セッター(age)メソッド
	def age=(i)
		@age = i
	end

end

#インスタンス作成
tomo = Student.new("佐々木智弘" ,27)
mari = Student.new("原田真里"   ,26)

#ゲッター確認
puts "名前：#{tomo.name}　、年齢：#{tomo.age}"
puts "名前：#{mari.name}　、年齢：#{mari.age}"


#セッター確認
tomo.name = "智弘"
tomo.age  = 20

mari.name = "真里真里"
mari.age  = 16

puts "名前：#{tomo.name}　、年齢：#{tomo.age}"
puts "名前：#{mari.name}　、年齢：#{mari.age}"
